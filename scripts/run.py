#!/usr/bin/env python3

import speech_recognition as sr
import time
import rospy
import rospkg
import signal
import sys
import yaml
from voice_recognition.srv import Command, CommandRequest
from actionlib_msgs.msg import GoalID
import gtts
from playsound import playsound
from threading import Thread
import threading
from tempfile import NamedTemporaryFile


def signal_handler(sig, frame):
    print("You pressed Ctrl+C!")
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

r = sr.Recognizer()
for index, name in enumerate(sr.Microphone.list_microphone_names()):
    print(
        'Microphone with name "{1}" found for `Microphone(device_index={0})`'.format(
            index, name
        )
    )
    if name == "default":
        mic_index = index


m = sr.Microphone(mic_index)
with m as source:
    r.adjust_for_ambient_noise(
        source, duration=1
    )  # we only need to calibrate once, before we start listening

# Some params specific to the microphone and environment
r.pause_threshold = 0.6
r.energy_threshold = 400
r.dynamic_energy_threshold = False
event_obj = threading.Event()


def callback(recognizer, audio):  # this is called from the background thread
    try:
        speech_as_text = recognizer.recognize_google(audio, language="fr-FR")
        recognize_main(speech_as_text)

    except sr.UnknownValueError:
        pass


def send_action(event_obj, timeout, action):
    # This thread waits `timeout` seconds if no flag has been raised it send the ros service
    flag = event_obj.wait(timeout)
    if flag:
        print("Cancelling action")
        event_obj.clear()
    else:
        print("Sending action")
        send_vocal_command(action)


def recognize_main(speech_as_text):
    # Detect one of the keywords,
    print('Recognized keyword in sentence : " ' + speech_as_text + ' "')
    # If recognize speak the corresponding reply
    speech_split = speech_as_text.split()
    if "non" in speech_split:
        # generating the event
        event_obj.set()
        gtts.gTTS(text="Action annulée", lang="fr").write_to_fp(
            voice := NamedTemporaryFile()
        )
        playsound(voice.name)
        voice.close()

    else:
        for k, v in keywords["keywords"].items():
            for res in speech_split:
                if res in k:
                    speech = keywords["keywords"][k][0]["reply"]
                    gtts.gTTS(text=speech, lang="fr").write_to_fp(
                        voice := NamedTemporaryFile()
                    )
                    # If stop directly stop the action
                    if res == "stop":
                        playsound(voice.name, False)
                        stop = GoalID()
                        stop_pub.publish(stop)
                    else:
                        playsound(voice.name, True)
                    # starting the thread who will wait 1.5s for the event
                    thread1 = threading.Thread(
                        target=send_action, args=(event_obj, 1.5, k)
                    )
                    thread1.start()
                    voice.close()


def start_recognizer():
    print("Listening in background")
    # Listening in the background
    r.listen_in_background(source, callback)

    time.sleep(1000)
    print("Sleep over, quitting")


def send_vocal_command(command):
    rospy.wait_for_service("vocal_command")
    try:
        send_vocal_command_srv = rospy.ServiceProxy("vocal_command", Command)
        voice_command = CommandRequest()
        voice_command.voice_command = command

        send_vocal_command_srv(voice_command)
    except rospy.ServiceException as e:
        print("Service call failed: %s" % e)


if __name__ == "__main__":
    rospy.init_node("voice_recognition")
    # Open yaml description files
    rospack = rospkg.RosPack()
    stop_pub = rospy.Publisher(
        "/panda/execute_trajectory/cancel", GoalID, queue_size=10
    )
    yaml_file_path = rospack.get_path("voice_recognition") + "/config/keywords.yaml"
    with open(yaml_file_path) as f:
        keywords = yaml.safe_load(f)
    # Start main script
    start_recognizer()
