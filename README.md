# voice recognition 

This library uses python to translate speech to text.

It also responds to the user according to what it heards.

It then send the corresponding action to the `solvay_state_machine` using ROS services

# Installation

```bash
pip install SpeechRecognition gTTS
```

# How to use 


```bash
rosrun voice_recognition run.py
```

# Other info

The `keywords.yaml` in the `config` folder is used to define the response to give according to specific keywords